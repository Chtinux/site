# Site internet : Chtinux reloaded 🍟🍟🍟🍟🍟🍟

- La prod : https://chtinux.org
- La pre-prod : https://chtinux.web.deuxfleurs.fr

Ce site est un site statique généré grace à [HUGO](https://gohugo.io/)

## 🖼️ Template du site

Le template est [hugoplate](https://themes.gohugo.io/themes/hugoplate/)
Un conseil bien lire le README du templates contient des particularités,
la documentation d'hugo ne suffit pas.

## 📅 Mettre à jour de l'agenda

Un script python récupere l'ics de l'agenda du libre et génére les pages hugo
(dans le dossier /content/francais/agenda/)

    ./recup_agenda.sh

Le script fonctionne avec python3, et dépend du paquet `icalendar`.


## Édition des DNS

Si vous voulez que le domaine chtinux.org pointe vers le vieux hébergement : `@ A 213.36.253.12`

Si vous voulez qu'il pointe vers Deuxfleurs : `@ ALIAS garage.deuxfleurs.fr.`

Déconnez pas, ya pas TLS sur le vieux bouzin, et sur le nouveau ya [HSTS](https://fr.wikipedia.org/wiki/HTTP_Strict_Transport_Security), qui explique à qui veut l'entendre que si le site est en HTTP (ou ses sous-domaines, comme le wiki), c'est que vous subissez une attaque. 

(En d'autres termes on est dans la merde pour le wiki.)
