---
title: "Contact"
meta_title: "contact"
description: "Contact chtinux"
draft: false
---

Courriel
--------

Pour nous joindre par email le plus simple et d'écrire à l'un des membres du collectif. 
Toutes les adresses électroniques se trouve sur la page de [présentation du collectif](/le_collectif.html),
elles sont aussi affichées sur la fiche de chaque membre. 


Réseaux sociaux
---------------

Nous sommes présents principalement sur les réseaux libres et fédérés suivants :

*   Mastodon : [chtinux@framapiaf.org](https://framapiaf.org/@Chtinux)



AFK - Away From Keyboard (Loin du clavier)
------------------------------------------

Vous pouvez aussi tout simplement venir à nos évènements ou à [nos permanences](/agenda.html) :-\]

