---
title: CLISSXXI
email: contact@cliss21.com
image: "/images/clissxxi.png"
description: une SCIC de service en informatique libre
website: https://www.cliss21.com/site/

---

SCIC (société coopérative d’intérêt collectif), installée à Liévin ainsi qu’à Lille.
Accompagne le développement technologique des PME-PMI, des collectivités territoriales et des associations de la région via l’utilisation et le développement de Logiciel Libre.
Organise des conférences et install party.
