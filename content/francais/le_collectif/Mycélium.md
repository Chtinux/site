---
title: Mycélium
email: contact@mycelium-fai.org
image: "/images/mycellium.png"
description: Fournisseur d'accès internet
website: https://mycelium-fai.org

---

L’association a pour objectif de fournir un accès Internet à ses membres et abonnés. Elle est rattachée à la FFDN. Mycélium s’organise de façon autogérée afin de ne pas laisser la gestion du réseau internet entre les mains d’intérêts privés et commerciaux.
Elle anime parfois des ateliers de sensibilisation à ces enjeux dans une optique d’autodéfense numérique.
