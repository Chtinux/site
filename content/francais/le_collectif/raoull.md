---
title: Raoull
email: bonjour@raoull.org
image: "/images/raoull.png"
description: "Fournisseur de services sur le World wide Welsh, membre du collectif CHATONS." 
website: https://www.raoull.org/

---

RAOULL est un collectif d’une dizaine de personnes qui depuis fin 2018 s’organise pour mettre en place des 
services sur internet éthiques à destination des gens et des associations. 
Les services proposés sont propulsés à base de logiciel libre.
