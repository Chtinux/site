---
title: CLX
email: contact@clx.asso.fr
image: "/images/clx.png"
description: Groupe d’Utilisateurs de Logiciels Libres actif sur le Nord et le Pas-De-Calais.
website: https://clx.asso.fr

---

Le Club LinuX Nord-Pas de Calais (CLX) est une association loi de 1901, groupe d’Utilisateurs de Logiciels Libres actif
de Wimille à Fourmies. L’association organise de nombreux évènements sur toute la région Nord-Pas De Calais: install
party, sensibilisation aux logiciels libres, réduction de la fracture numérique et bien d'autres.
