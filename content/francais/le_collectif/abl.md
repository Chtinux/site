---
title: ABBL
email: abl[at]univ-lille.fr
image: "/images/abl.png"
description: Association des Bidouilleurs et Bidouilleuses Libristes
website: https://bidouilleurslibristes.github.io/

---

L’Association des Bidouilleurs et Bidouilleuses Libristes (ABL) est une association loi 1901 qui a pour objet l’hébergement de projets libres, la promotion des communs et la promotion du libre (au sens licence libre).
