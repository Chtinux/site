---
title: Echos de gnous
email: edg@chtinux.org
image: "/images/echo_des_gnous.jpg"
description: "Emission de radio de vulgarisation et dactualités sur le logiciel libre"
website: https://www.echodesgnous.org/
---


L’Écho des Gnous est l’émission de radio consacrée au logiciel Libre et à la culture libriste, animée par l’association Chtinux. Elle est diffusée dimanche soir de 19h à 20h, sur Radio Campus Lille, 106.6 FM sur la métropole lilloise ou en streaming sur campuslille.com.

Depuis octobre 2010 nous vous proposons « l’émission qui vous explique l’informatique libre », pas réservée aux informaticiens, mais au contraire destinée au plus grand nombre. Nous vous proposons deux concepts en alternance, chacun étant diffusé une semaine sur deux :

- la Face A traite de l’actualité informatique sous l’angle du Logiciel Libre, de la sécurité informatique, du respect de la vie privée et de la liberté d’expression sur Internet. La rubrique dégeekalisation a pour but de démystifier un sujet technique supposé complexe, mais qui gagnerait à être connu de tous ;
- la Face B, sorte de dégeekalisation étendue, développe chaque semaine un sujet pendant toute l’heure de l’émission. Devenez incollables sur les sujets de fond de la culture libriste !

Quelques pauses musicales sont proposées pendant l’émission. Bien entendu, tous les morceaux choisis sont disponibles sous licences de libre diffusion (Creative Commons, Art Libre…)

Vous pouvez contacter l’équipe de l’émission via IRC (webchat) ou par email en écrivant à e d g CHEZ c h t i n u x POINT o r g.

Envie de retrouver les précédentes émissions ?

Vous trouverez toutes les informations sur notre site : https://www.echodesgnous.org/ 

Vous pouvez consulter la liste complète des émissions diffusées, ainsi que leurs contenus sur notre wiki.

