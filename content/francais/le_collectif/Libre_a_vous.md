---
title: Libre à vous
email: 
image: "/images/omjc.png"
description: Atelier d'initialisation à l'informatique libre
website: https://www.omjc-info.fr/Libre-a-Vous
social: 
  - name: phone
    icon: fas fa-phone
    link: tel:03 28 80 54 25

---

Rendez-vous hebdomadaire à la ferme Dupire (Villeneuve d'Ascq), le samedi de 9h à 12h.
Organisé par l'OMJC (Observatoire des Mutations de la Jeunesse et de la Citoyenneté).
Ces ateliers vous permettront de vous familiariser avec les systèmes et les logiciels libres.
