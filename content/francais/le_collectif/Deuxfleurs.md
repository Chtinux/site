---
title: Deuxfleurs
email: coucou@deuxfleurs.fr
image: "/images/deuxfleurs.svg"
description: Association œuvrant à la construction d'un Internet convivial, membre du collectif CHATONS.
website: https://deuxfleurs.fr/

---
Deuxfleurs héberge des services numériques fondamentaux (site web, e-mail, chat, visio, blog...) 
pour tout type d'usager⋅e. 

Notre démarche est écologique et citoyenne : notre infrastructure est constituée de vieilles machines hébergées 
chez nos membres ; notre gouvernance est collégiale, et promeut le respect des droits humains. 
