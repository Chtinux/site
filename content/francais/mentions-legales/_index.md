---
title: "Mentions légales"
meta_title: "Mentions légales"
description: "Mentions légales du site Chtinux!"
draft: false
---
# Identification

Le site chtinux.org est édité collectivement par ses membres.

Les membres sont joignable par courrier électronique, toutes les adresses électroniques de contact 
se trouve sur la page de [présentation du collectif](/le_collectif.html).
# Hébergement

Ce site est hébergé en France par le chatons Deuxfleurs sur ses serveurs à Lille.


# Informatique et libertés

Aucune donnée personnelle n’est collectée lors de la navigation sur ce site. 
Nous n’effectuons aucune étude statistique des visites. 

