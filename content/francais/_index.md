---
# Banner
banner:
  title: "Pour un numérique bien free!"
  content: "Chtinux est un collectif œuvrant pour les libertés numériques dans la région lilloise et ses alentours."
  image: "/images/logo_chtinux.svg"
  button:
    enable: false
    label: "Get Started For Free"
    link: "#qui_sommes_nous"

# Features
features:
  - title: "Qui sommes nous ?"
    image: "/images/frite.svg"
    image_alt: "La flamme de la liberté"
    content: "Chtinux est un collectif œuvrant pour les libertés numériques dans la région lilloise et ses alentours. Notre collectif tient des permanences au Café Citoyen chaque dernier mardi du mois. Nous participons ponctuellement à d’autres évènements"
   
    button:
      enable: true
      label: "En savoir plus"
      link: "/apropos.html"





# Testimonials



---
