---
title: "Découvrir Zourit, la suite logiciels pour les associations"
date_evt: "2025-03-18 10:00:00+01:00"
location: "15 rue René Lanoy, Lens, Hauts-de-France, France"
ville: "Lens"
url_info: "https://letoitcommun.org/evenements/atelier-num%C3%A9rique-zourit"
---

Dans le cadre du dispositif Guid'Asso, le Toit Commun propose un atelier de présentation de Zourit, une suite intégrée de logiciels permettant de travail de façon collaborative dans vos collectifs et associations.

 Il est maintenu par les CEMEA, qui seront présents lors de cet atelier afin de présenter leur outil.



En attendant, retrouver ici une présentation du projet: https://zourit.net/

