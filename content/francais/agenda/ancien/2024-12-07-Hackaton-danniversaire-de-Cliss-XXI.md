---
title: "Hackaton d'anniversaire de Cliss XXI"
date_evt: "2024-12-07 09:00:00+01:00"
location: "16 allée de la Filature, Lille, Hauts-de-France, France"
ville: "Lille"
url_info: "https://www.cliss21.com/site/educ-populaire/les-20-ans-de-cliss-xxi/article/hackathon-du-7-8-decembre"
---

Pour leur 20 ans, Cliss XXI, coopérative de service informatique en logiciel libre, vous invite à contribuer à un logiciel libre dans le cadre d’un hackathon, le week-end du 7/8 décembre à la coroutine, 16 allée de la Filature à Lille.



Il sera proposé d’explorer plusieurs logiciels:

* Contribuez à une nouvelle version de [Framadate][1], l’application qu’on ne présente plus, pour trouver simplement un créneau de réunion.
* Plongez-vous dans le Fediverse en créant une application vraiment décentralisée, en ActivityPub, à l’aide du Framework [ActivityPods][2].
* Aidez-nous à documenter [Bénévalibre][3], le logiciel de suivi du temps bénévole dans les associations, et découvrez sa nouvelle version avant sa sortie.
* Découvrez, testez et débusquez des bugs dans [Paheko][4], la solution de gestion d’association simple, complète et efficace.

Et d’autres ateliers :

* Discussions sur le financement des communs
* Atelier d’auto-défense numérique
* ...

C’est ouvert à tout·e·s (sur réservation): développeur·ses expérimenté·e, geek passionné·e, simple utilisateur·ice motivé·e... venez quel que soit votre niveau de compétence technique, vous serez accompagné·e. Vous pouvez également si vous le souhaitez proposer un atelier ou une discussion.

🍽 Repas inclus sur place

Sur inscription: [https://framaforms.org/inscription-au-hackathon-du-78-decembre-1731404249][5]



[1]: https://framadate.org/
[2]: https://activitypods.org/
[3]: https://benevalibre.org/
[4]: https://paheko.cloud/
[5]: https://framaforms.org/inscription-au-hackathon-du-78-decembre-1731404249
