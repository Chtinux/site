---
title: "Install-Party Linux & Logiciels libres"
date_evt: "2024-12-03 19:00:00+01:00"
location: "3 place des Martyrs, Croix, Hauts-de-France, France"
ville: "Croix"
url_info: "http://clx.asso.fr"
---

Votre ordinateur est lent ou fatigué?
Il fonctionne moins bien et vous aimeriez retrouver un fonctionnement adapté?

Vous pouvez lui donner un nouveau coup de santé!
Emmenez le (prenez soin de sauvegarder vos documents précieux avant de sortir), nous faisons le reste avec vous!

Au cours de la séance, nous vous proposons d'installer le système d'exploitation libre Linux et/ou les logiciels libres que vous utilisez sur votre ordinateur.

