---
title: "Repair Café de Orchies"
date_evt: "2024-11-16 14:00:00+01:00"
location: "9 rue Jules Ferry, Orchies, Hauts-de-France, France"
ville: "Orchies"
url_info: "https://clx.asso.fr"
---

Afin de limiter la surconsommation numérique et lutter contre l’obsolescence programmée des systèmes d’exploitation, l'association CLX propose d’installer un système de remplacement gratuit, en français et sans publicité qui vous permettra de continuer à l’utiliser avec tout le nécessaire pour retrouver vos usages du quotidien (navigation internet, envoi de mail, suite bureautique, vidéos, retouche photos...).

[Plus de détails sur OpenAgenda][1]



[1]: https://repaircafeenpevele.fr/?oaq%5Buid%5D=17910887
