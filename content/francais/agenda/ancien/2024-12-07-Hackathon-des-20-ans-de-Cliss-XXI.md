---
title: "Hackathon des 20 ans de Cliss XXI"
date_evt: "2024-12-07 10:00:00+01:00"
location: "16 Allée de la Filature, Lille, Hauts-de-France, France"
ville: "Lille"
url_info: "https://www.cliss21.com/site/educ-populaire/les-20-ans-de-cliss-xxi/article/20-ans-de-cliss-xxi"
---

Dans le cadre de ces 20 ans, la coopérative Cliss XXI organise un Hackathon !



Développeur·euses chevronné·es ou novices enthousiastes, rendez-vous pour 2 jours de contribution collective sur des projets libres pensés par et pour des associations.

Repas et connexion fournis ; ambiance garantie pour ce hackathon !

(Retrouvez plus d’informations sur le site de [cliss21.com][1])



[1]: https://www.cliss21.com/site/educ-populaire/les-20-ans-de-cliss-xxi/article/20-ans-de-cliss-xxi
