---
title: "[Non-mixité] Chtitedev : le Libre et l'Open Source"
date_evt: "2025-01-14 18:30:00+01:00"
location: "82 rue Winston Churchill, Lille, Hauts-de-France, France"
ville: "Lille"
url_info: "https://mobilizon.fr/events/eed91b7c-09f4-4d09-ba73-85d1ea02e5e0"
---

Événement Chtitedev

Chtitedev est un collectif de femmes de la tech (développeuses, devOps, data eng,…) qui se réunissent mensuellement sous forme de meetup en non-mixité choisie.



Vous êtes une femme cis, une personne trans ou non binaire ? Vous entretenez un intérêt pro ou perso pour la programmation ?

Nous vous proposons de nous réunir afin de nous entraider pour pouvoir avancer au mieux dans ce monde qui a du mal à nous réintégrer dans ses rangs !

Au programme: ⏰ 18h30: Arrivée, petit tour de table et papotage

⏰ 18h45: Présentation + discussion: Le libre et l'open source... penchons-nous ce soir sur un sujet à la fois historique, technique et politique qui divise le milieu informatique depuis plus de 50 ans. On passera rapidement sur les origines, pour ensuite parler des projets les plus connus, de ce que le les logiciels libres peuvent vous apporter concrètement, et des endroits où aller sur Lille en tant que débutant si on veut commencer à libérer son informatique. A la fin vous aurez une liste concrète de logiciels testés et approuvés que vous pourrez utiliser sur le champ dans votre vie quotidienne ou dans votre vie pro. Pour respecter le thème de cette rencontre tous les logiciels utilisés pour l'inscription et l'annonce de cet événement seront libres. Une présentation de Marie Dubremetz, libriste enthousiaste, membre de l'association raoull et régulière des permanences du Libre au café citoyen.

⏰ 19h45: On propose des idées pour notre prochaine rencontre en choisissant ensemble un sujet.

⏰ 20h15: Jeux de société, apéro ou autres en fonction de nos envies. 🎲🍹

Nos rencontres se déroulent à Lille, mais si vous êtes d'une autre région ou que vous ne pouvez pas vous déplacer, rejoignez-nous en ligne !

Les rencontres sont en non mixité choisie, que ce soit sur place ou en visio. Si vous n'êtes pas une femme, personne trans ou non binaire, merci de ne pas vous inscrire.

Accès en ligne: [https://jitsi.deuxfleurs.fr/chtitedev][1]

Accessibilité PMR: oui

Inscription obligatoire: voir lien d'inscription framadate sur mobilizon

Rendez-vous chez [Les-Tilleuls.coop][2] situé à Euratechnologies mais pas dans le bâtiment principal:

82 rue Winston Churchill

3e étage

59160 Lille

+33 3 66 72 43 94

Une fois arrivées sur place, **sonnez au 31** sur l'interphone et un membre de notre équipe vous accueillera.

Pour en savoir plus sur notre démarche, consultez notre page Mobilizon: [https://mobilizon.fr/@chtitedev][3]

Pour contacter les organisatrices, envoyez un email à: chtitedev-request@lists.fripost.org

Retrouvez-nous sur LinkedIn

Let's go girls !

Nous remercions [Les-Tilleuls.coop][4] pour l'hébergement de ce meetup et [deux-fleurs.fr ][5]pour l'hébergement en ligne.



[1]: https://jitsi.deuxfleurs.fr/chtitedev
[2]: http://Les-Tilleuls.coop
[3]: https://mobilizon.fr/@chtitedev
[4]: https://les-tilleuls.coop/
[5]: https://deuxfleurs.fr/
