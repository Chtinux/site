---
title: "Mercredis Linux"
date_evt: "2025-01-08 19:30:00+01:00"
location: "16 rue de la Ladrerie, Cappelle en Pévèle, Hauts-de-France, France"
ville: "Cappelle en Pévèle"
url_info: "http://clx.asso.fr"
---

[L'Association Club Linux Nord Pas de Calais][1] organise chaque mois une permanence *Logiciels Libres* ouverte à tous, membre de l'association ou non, débutant ou expert, curieux ou passionné.



Les Mercredi Linux sont des réunions mensuelles désormais organisées le mercredi. Ces réunions sont l’occasion de se rencontrer, d’échanger des idées ou des conseils.

Régulièrement, des présentations thématiques sont réalisées lors de ces réunions, bien sûr, toujours autour des logiciels libres.

Durant cette permanence, vous pourrez trouver des réponses aux questions que vous vous posez au sujet du Logiciel Libre, ainsi que de l'aide pour résoudre vos problèmes d'installation, de configuration et d'utilisation de Logiciels Libres. N'hésitez pas à apporter votre ordinateur, afin que les autres participants puissent vous aider.

Cette permanence a lieu à la [Médiathèque Cultiv'Art](/\%22https:/mediatheques.pevelecarembault.fr/mediatheque-cappelle-en-pevele/#1494853048594-67fa4d77-24830a45-654bc562-0ee4\%22), 16 rue de la Ladrerie, Cappelle en Pévèle



[1]: http://clx.asso.fr
