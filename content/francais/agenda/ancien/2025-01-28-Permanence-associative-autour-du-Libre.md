---
title: "Permanence associative autour du Libre"
date_evt: "2025-01-28 20:00:00+01:00"
location: "7 place du Vieux Marché aux Chevaux, Lille, Hauts-de-France, France"
ville: "Lille"
url_info: "https://chtinux.org"
---

Vous avez décidé de reprendre en main votre vie numérique? Venez nous rencontrer **le dernier mardi de chaque mois au [Café Citoyen][1]** à Lille!

La permanence associative autour du Libre est une manifestation conviviale, ouverte à toutes et tous, organisée le dernier mardi (ou jeudi) de chaque mois par les collectifs de [Chtinux][2] ([Raoull,][3] [Deuxfleurs][4], [Mycélium][5], [CLX][6], [Cliss XXI][7],...).

Rejoignez-nous pour y discuter joyeusement de Logiciel Libre, de Culture Libre, de données ouvertes (*open data*), de bidouille sous Linux, ou proposer vos idées d’évènements.

C'est aussi l'occasion d'obtenir un coup de main si vous rencontrez une difficulté sous Linux, ou si vous avez besoin de conseils pour migrer sur du Logiciel Libre.

Si vous venez avec votre ordinateur pour obtenir de l'aide technique, pour permettre à l'équipe bénévole de s'organiser, prévenez-nous via un courrier électronique à l'adresse: chtinux-diffusion CHEZ deuxfleurs POINT fr.

Le Café Citoyen est accessible en métro (station République - Beaux Arts). Une connexion Internet y est disponible, des prises électriques, de la place... Au bar, vous trouverez aussi de bonnes boissons avec et sans alcool, ainsi que de la petite restauration (notamment fromage ou tartines véganes).

Pour soutenir le Café Citoyen, nous vous demandons d'y acheter au minimum une consommation. Après avoir pris votre boisson ou votre en-cas au bar, vous pouvez nous rejoindre directement au deuxième étage.

Au plaisir de vous retrouver!



[1]: https://cafecitoyen.org/
[2]: http://chtinux.org/
[3]: https://raoull.org/
[4]: https://deuxfleurs.fr
[5]: https://mycelium-fai.org/wiki/
[6]: http://clx.asso.fr/
[7]: https://www.cliss21.com/site/
