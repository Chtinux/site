---
title: "Apéro OSM Lille"
date_evt: "2024-11-26 17:30:00+01:00"
location: "7 place du marché aux chevaux, Lille, Hauts-de-France, France"
ville: "Lille"
url_info: ""
---

Une rencontre autour d'un verre pour échanger sur nos pratiques et les divers actualités !

N'hésitez pas à [répondre à ce sondage][1] si vous souhaitez venir.

Cet apéro s'enchainera avec la permanence du libre Chtinux.



[1]: https://framadate.org/1ygaMfhf6MNTuF7l
