---
title: "Numérique libre au village solidaire de la braderie"
date_evt: "2024-09-14 10:00:00+02:00"
location: "place du Vieux Marché aux Chevaux, Lille, Hauts-de-France, France"
ville: "Lille"
url_info: ""
---

**Numérique libre au village solidaire de la braderie**

Plusieurs collectifs d'informatique libre œuvrant dans la métropole se joignent au Café Citoyen pour tenir un stand d'information durant la grande braderie, qui aura lieu le week-end du 14 et 15 septembre, sur la Place du Vieux Marché aux Chevaux.

Parmi les collectifs présents à notre stand:

* Chtinux: groupe d'utilisateurs et utilisatrices de logiciels libres
* ClissXXI: coopérative d'informatique libre, social et solidaire
* CLX: groupe d'utilisateurs et utilisatrices de logiciels libres
* Mycélium: association travaillant à la mise en place d'un Fournisseur d'Accès Internet, membre de la Fédération FFDN
* Raoull: association œuvrant à la mise en place de services internet éthiques, membre du collectif CHATONS
* Deuxfleurs: fournisseur de services en ligne libres, sobres et non-marchands, membre du collectif CHATONS

Que vous soyez un(e) geek déjà convaincu(e), ou au contraire que vous trouviez l'outil trop emprisonnant et cherchiez des échappatoires, n'hésitez pas à passer à notre stand pour vous informer, nous rencontrer en chair et en os, voire récupérer quelques goodies.

Bonne braderie !

