---
title: "Ateliers Logiciels Libres"
date_evt: "2024-09-21 09:00:00+02:00"
location: "OMJC, rue Yves Decugis, Villeneuve d’Ascq, Hauts-de-France, France"
ville: "Villeneuve d’Ascq"
url_info: "http://www.omjc-info.fr"
---

L'[OMJC][1], en partenariat avec [l'Association Club Linux Nord Pas de Calais, ][2]organise chaque samedi, **Libre à Vous**, une permanence *Logiciels Libres* ouverte à toustes, débutant ou expert, curieux ou passionné.

Vous souhaitez tester **GNU/Linux** sur votre ordinateur, vous recherchez un logiciel pour une fonction précise, des conseils ou de l'aide sur les **logiciels libres**?

Libre à Vous est une permanence destinée à vous faciliter l'utilisation de l'informatique. Vous repartirez avec «le plein» de **logiciels libres**, fiables, évolutifs, performants et gratuits.



Ça se déroule chaque samedi matin (*hors vacances scolaires*) au Centre d'Infos Jeunes, à la ferme Dupire, 80 rue Yves Decugis à Villeneuve d'Ascq (*métro Triolo*) de 9h00 à 12h00.

Entrée Libre. Tout Public.



[1]: https://www.omjc-info.fr/atelier-prtic
[2]: http://clx.asso.fr
