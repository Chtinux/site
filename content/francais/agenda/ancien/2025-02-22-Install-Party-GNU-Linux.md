---
title: "Install Party GNU Linux"
date_evt: "2025-02-22 15:00:00+01:00"
location: "Médiathèque Marguerite Yourcenar, 199 rue carnot, Faches-Thumesnil, Hauts-de-France, France"
ville: "Faches-Thumesnil"
url_info: "https://link.infini.fr/pagefbdesole"
---

Install Party GNU Linux



Organisée par un bénévole passionné dans le cadre du dispositif Ambassadeur de la Médiathèque.

Le temps d'un après-midi à la Médiathèque Marguerite Yourcenar de Faches Thumesnil, on redonne une vie à votre ancien ordinateur et on le libère des GAFAM.

**Inscription et prérequis** sur [https://framadate.org/65ixnQE9PtCOANFi][1] et sur place à la médiathèque, places limitées.

Nota: *pour les prochaines, les bénévoles intéressés sont les biens venus*





[1]: https://framadate.org/65ixnQE9PtCOANFi
