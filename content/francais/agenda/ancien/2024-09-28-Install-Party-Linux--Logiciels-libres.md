---
title: "Install-Party Linux & Logiciels libres"
date_evt: "2024-09-28 10:00:00+02:00"
location: "2 rue Pierre Motte, Roubaix, Hauts-de-France, France"
ville: "Roubaix"
url_info: "http://www.mediathequederoubaix.fr/agenda/booster-son-pc-linux-2"
---

Votre ordinateur est lent ou fatigué?

Il fonctionne moins bien et vous aimeriez retrouver un fonctionnement adapté?

Vous pouvez lui donner un nouveau coup de santé!

Emmenez le (prenez soin de sauvegarder vos documents précieux avant de sortir), nous faisons le reste avec vous!

Au cours de ces séances, nous vous proposons d'installer le système d'exploitation libre Linux et/ou les logiciels libres que vous utilisez sur votre ordinateur.

Nous sommes aussi présent aux Petites Cantines à Croix chaque premier mardi du mois.

