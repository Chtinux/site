---
title: "Table ronde : 20 ans de communs numériques, enjeux et perspectives"
date_evt: "2024-11-07 14:30:00+01:00"
location: "24 rue Paul Sion, Lens, Hauts-de-France, France"
ville: "Lens"
url_info: "https://www.cliss21.com/site/educ-populaire/les-20-ans-de-cliss-xxi/article/20-ans-de-cliss-xxi"
---

Dans le cadre des 20 ans de Cliss XXI, nous proposons une table ronde en présence de représentants
de collectivités territoriales, d’acteurs du logiciel libre et d’une chercheuse, spécialiste des communs numériques.



* 14H30 Présentation de solutions en logiciels libres pour les collectivités et les structures de l’ESS.
* 17H « 20 ans de communs numériques, enjeux et perspectives »: Table-ronde en présence de représentants de collectivités territoriales, d’acteurs du logiciel libre et d’une chercheuse, spécialiste des communs numériques.

Le programme complet de nos événements sur notre site web: [https://www.cliss21.com/site/educ-populaire/les-20-ans-de-cliss-xxi/article/20-ans-de-cliss-xxi][1]





[1]: https://www.cliss21.com/site/educ-populaire/les-20-ans-de-cliss-xxi/article/20-ans-de-cliss-xxi
