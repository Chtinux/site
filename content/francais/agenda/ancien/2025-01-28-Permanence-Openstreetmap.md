---
title: "Permanence Openstreetmap"
date_evt: "2025-01-28 18:00:00+01:00"
location: "7 place du Vieux Marché aux Chevaux, Lille, Hauts-de-France, France"
ville: "Lille"
url_info: ""
---

Une rencontre autour d'un verre des contributeur Openstreetmap ou toutes personnes intéressées et souhaitant se lancer, pour échanger sur nos pratiques et les divers actualités!

[N'hésitez pas à répondre à ce sondage][1] pour savoir combien de personnes seront présentes.

Cet apéro s'enchainera avec la permanence du libre Chtinux.

Pour soutenir le Café Citoyen, nous vous demandons d'y acheter au minimum une consommation. Après avoir pris votre boisson ou votre en-cas au bar, vous pouvez nous rejoindre directement au deuxième étage.



[1]: https://framadate.org/JMwdRCruPbGmqVZX
