---
title: "Repair Café de Moncheaux"
date_evt: "2025-01-18 09:30:00+01:00"
location: "20 rue de Bouvincourt, Moncheaux, Hauts-de-France, France"
ville: "Moncheaux"
url_info: "http://clx.asso.fr"
---

Afin de limiter la surconsommation numérique et lutter contre l’obsolescence programmée des systèmes d’exploitation, l'association CLX propose d’installer un système de remplacement gratuit, en français et sans publicité qui vous permettra de continuer à l’utiliser avec tout le nécessaire pour retrouver vos usages du quotidien (navigation internet, envoi de mail, suite bureautique, vidéos, retouche photos...).

[Plus de détails sur OpenAgenda][1]



[1]: https://repaircafeenpevele.fr/?oaq%5Buid%5D=20457679%22
