---
title: "Mercredis Linux"
date_evt: "2025-04-23 18:30:00+02:00"
location: "311 rue Salvador Allende, Cysoing, Hauts-de-France, France"
ville: "Cysoing"
url_info: "https://clx.asso.fr"
---

[L'Association Club Linux Nord Pas de Calais][1] organise chaque mois une permanence *Logiciels Libres* ouverte à tous, membre de l'association ou non, débutant ou expert, curieux ou passionné.



Durant cette permanence, vous pourrez trouver des réponses aux questions que vous vous posez au sujet du Logiciel Libre, ainsi que de l'aide pour résoudre vos problèmes d'installation, de configuration et d'utilisation de Logiciels Libres.

N'hésitez pas à apporter votre ordinateur, afin que les autres participants puissent vous aider.

Dans une salle équipée d'un tableau blanc et d'un vidéoprojecteur, se dérouleront fréquemment des ateliers, des initiations, des discussions, des tests, des démonstrations, de l'entraide abordant le **logiciel libre** et de la dégustation de bières.

Cette permanence a lieu à [l'EPN (Espace Public Numérique)][2], 311 rue Salvador Allende à Cysoing.



[1]: http://clx.asso.fr
[2]: https://epn.pevelecarembault.fr/epn-de-cysoing/#1541584407821-aea5a232-d5cf17d7-34ef7c86-0724/
