---
title: "Atelier Local-Low-Tech"
date_evt: "2025-03-04 19:00:00+01:00"
location: "3 place des Martyrs, Croix, Hauts-de-France, France"
ville: "Croix"
url_info: "https://clx.asso.fr"
---

[L'Association Club Linux Nord Pas de Calais][1] est présent tous les premiers mardis du mois aux Petites Cantines, à Croix.

Au cours de ces séances, nous vous proposons d'installer le système d'exploitation libre Linux et/ou les logiciels libres que vous utilisez sur votre ordinateur.



Si votre ordinateur est récent et que vous vous voulez vous donner les moyens de maîtriser les informations qui y entrent et en sortent, ou si votre ordinateur devient poussif...

Pensez à nous rendre visite, c'est gratuit et on vous donnera toutes les clés pour que vous puissiez faire le choix qui vous convient 😁

Cette manifestation a lieu aux [Petites Cantines de Croix][2]. au 3 Place des Martyrs de la résistance, Croix.



[1]: https://clx.asso.fr
[2]: https://lille.lespetitescantines.org
