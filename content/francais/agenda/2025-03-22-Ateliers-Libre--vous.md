---
title: "Ateliers Libre à vous"
date_evt: "2025-03-22 09:00:00+01:00"
location: "OMJC, rue Yves Decugis, Villeneuve d’Ascq, Hauts-de-France, France"
ville: "Villeneuve d’Ascq"
url_info: "https://clx.asso.fr"
---

L'[OMJC][1] organise avec [l'Association Club Linux Nord Pas de Calais][2] organise chaque samedi une permanence *Logiciels Libres* ouverte à tous, membre de l'association ou non, débutant ou expert, curieux ou passionné.



Le Centre d’Infos Jeunes a mis en place une démarche d’accompagnement des jeunes aux pratiques actuelles pour l’informatique et le numérique: \* Lieu d’accès public à Internet ( 5 postes avec Wifi libre et gratuit ) \* Web collaboratif et citoyen pour que chacun puisse trouver sa place et passer du rôle de simple usager à celui d’initiateur de processus collaboratif \* Éducation à l’information par les nouveaux médias ( diffusion par le biais du numérique ) \* Logiciels libres ( bureautique, sites, blogs, cloud, infographie et vidéo, musique, réseaux sociaux, chat, … ).

Cette rencontre a lieu sur rendez-vous, tous les samedis matins hors vacances scolaires à la Maison communale de la ferme Dupire, rue Yves Decugis à VILLENEUVE D’ASCQ



[1]: https://www.omjc-info.fr/atelier-prtic
[2]: http://clx.asso.fr
