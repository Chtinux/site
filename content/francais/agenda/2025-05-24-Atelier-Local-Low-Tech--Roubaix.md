---
title: "Atelier Local-Low-Tech à Roubaix"
date_evt: "2025-05-24 10:00:00+02:00"
location: "2 rue Pierre Motte, Roubaix, Hauts-de-France, France"
ville: "Roubaix"
url_info: "https://clx.asso.fr"
---

[L'Association Club Linux Nord Pas de Calais][1] est présent chaque trimestre à la médiathèque de Roubaix La Grande Plage.



Au cours de ces séances, nous vous proposons d'installer le système d'exploitation libre Linux et/ou les logiciels libres que vous utilisez sur votre ordinateur.

Si votre ordinateur est récent et que vous vous voulez vous donner les moyens de maîtriser les informations qui y entrent et en sortent, ou si votre ordinateur devient poussif...

Pensez à nous rendre visite, c'est gratuit et on vous donnera toutes les clés pour que vous puissiez faire le choix qui vous convient 😁

Cette manifestation a lieu à la[Médiathèque de Roubaix][2]. au 2, rue Pierre Motte à Roubaix



[1]: https://clx.asso.fr
[2]: http://www.mediathequederoubaix.fr/agenda/linux-install-party
