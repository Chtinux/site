---
title: "Chtinux!"
meta_title: "A propos"
description: "Chtinux est une association de promotion des logiciels libres de la métropole lilloise"
draft: false
masquer_liens: true
---
Chtinux est un collectif œuvrant pour les libertés numériques dans la région lilloise et ses alentours.

Initialement, Chtinux était une association fondée en 2002 sous le nom de Campux comme un groupe d’utilisateurs de Linux par des étudiants de l'Université Lille 1 (retrouvez tout l'historique, [ici](/apropos/histo.html) ). 
Depuis 2024, des personnes réactivent et transforment l'association en collectif.
Cette nouvelle forme a pour objet d'offrir une bannière commune pour réunir la diversité des acteurs du numérique libre de la région lilloise.

Notre collectif tient des permanences pour discuter, échanger entre nous, mais aussi accueillir et répondre aux curieux.
Elles ont lieu chaque dernier mardi du mois, à partir de 20h00, au Café Citoyen.

Nous participons ponctuellement à d’autres évènements comme la braderie de Lille.
