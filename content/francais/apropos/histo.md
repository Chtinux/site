---
title: "Il est un fois Chtinux!"
meta_title: "historique"
description: "Chtinux est une association de promotion des logiciels libres de la métropole lilloise"
draft: false
---

Voici l’histoire du groupe d’utilisateurs de logiciels libres Chtinux, anciennement Campux, depuis sa création en tant que LUG étudiant, jusque nos jours en tant que groupe de la métropole lilloise.

Notez : Chtinux sera parfois appelée Campux, en rappel de son nom historique au moment des évènements.

== Les aïeux ==

Campux (c’était le nom de l’association alors) n’est pas vraiment une association pionnière, les trois universités publiques de Lille sont depuis longtemps le théâtre de quelques activités autour du logiciel libre.

Pour preuve cette information sur LinuxFR et cette activité entre Lille1 et Lille2 en 2002. Aussi un courrier de Lille1 adressé à MandrakeSoft en 2003. Il existait aussi une association Lillux (promotion du logiciel libre éducatif) à Lille2, semble-t-il. Depuis longtemps le système GNU/Linux est utilisé dans les trois Universités. Dans le domaine de la recherche, les enseignants chercheurs utilisent depuis longtemps GNU/Linux. À Lille1, le BDE Association des Etudiants en Informatique (AEI) a semble-t-il était pro-linuxien à une époque, puisque cette association a comme emblème un Tux.

Enfin, on ne pourrait pas parler du logiciel libre à Lille sans parler du CLX, LUG fondé en 1998 par une dizaine de linuxiens à Seclin. Il a œuvré et continue œuvrer dans toute la région Nord-Pas-de-Calais.

== Le point de départ ==

Campux a probablement commencé par la rencontre de deux couples de linuxiens peu après la rentrée 2002. Jiel Beaumadier et Matěj Hausenblas d’une part, Julien Graziano (jux) et Julien Derveeuw (theturtle) d’autres part, font connaissance sur le canal IRC de Léa-Linux. Les premiers sont en DEUG maths, les seconds en maîtrise d’informatique : une rencontre est programmée au bâtiment M5 à Lille1. Les 4 manchots s’aperçoivent qu’ils ne sont pas plus tous seuls sur la banquise.

Le samedi 1ᵉʳ février 2003, Jiel et Matěj bravent la neige pour assister à une install party organisée sur Lille1 par des étudiants en DESS (entre autres Mikael Mourcia). Ils s’aperçoivent alors qu’il y a d’autres linuxiens sur la fac et dans les écoles d’ingénieurs, peu nombreux et isolés chacun dans leur formation. On peut considérer que c’est le premier événement auquel assista le futur groupe de Campux. 

== Campux : l’idée du LUG étudiant ==

Au cours d’une discussion sur IRC en juillet 2003, jux et Jiel abordent l’idée de création d’un LUG pour les étudiants de Lille. L’idée est séduisante, mais on est en vacances loin de l’Université, alors on y pense plus trop :-) Cependant l’idée continue de travailler dans les esprits, et le 8 août 2003 Jiel envoie un courriel à ses amis linuxiens pour les convier à créer un LUG universitaire.

À l’époque tout le monde ne répond pas, car beaucoup sont en vacances. Mais à la rentrée début septembre, l’équipe est motivée. On décide de faire une association loi 1901. Jiel écrit les statuts et ces derniers seront alors bien relus par tout le monde et certains points modifiés suite à une réunion sur IRC. C’est la première action démocratique de l’association ! Ensuite commencent de longues procédures administratives. Finalement la date officielle de déclaration de Campux à la préfecture de Police de Lille sera le 7 novembre 2003. Il faudra encore attendre quelque temps pour pouvoir être reconnu par l’Université de Lille1, où nous l’association était basée. C’est pendant ce temps que le site internet de Campux est fait par Jiel, très rapidement et assez mal codé en PHP, mais avec une apparence du gestionnaire de fenêtres WindowMaker. Le nom de Campux provient du mélange entre les mots “campus” et “Linux”, il a été trouvé par un proche de jux. Nous avions pensé aussi à divers noms, par exemple “Lillux”, mais le LUG du Luxembourg a déjà un nom similaire (à cette époque nous ne savions pas qu’il y avait déjà eu un “Lillux”). 

== Les débuts : 2003-2004 ==

Le premier conseil d’administration (CA) de Campux sera constitué comme suit : Jiel (président), jux (vice-président), Matěj (secrétaire), Benoit Gosse (trésorier), Anthoine Bourgeois (matériel). À ce moment-là, à part le CA, l’association ne compte qu’un seul membre qui est theturtle. Mais ce CA ne sera jamais opérationnel, et lors du début des activités de Campux en janvier 2004, l’équipe est un peu remaniée. Le nouveau CA sera composé de 4 personnes : Jiel (président), jux (vice-président), Matěj (trésorier) et Tony Ducrocq (secrétaire) qui vient tout juste de rejoindre l’association.

L’annonce de création de l’association Campux est rendue publique à Linux Solutions 2004 à Paris. L’accueil y est chaleureux. L’organisation de l’événement pour Campux sera organisée quasi entièrement par Matěj qui montrera qu’il est beaucoup plus qu’un simple trésorier :-) C’est également à cette période que Campux décide de reprendre les Mardi-Linux de Lille organisés initialement par le CLX, mais qui n’étaient plus très présents dans la capitale des Flandres.

À partir de ce moment Campux verra son activité et ses membres augmenter considérablement. L’association sera présente au FOSDEM à Bruxelles et aux RMLLs à Bordeaux, date de la première vente des lingettes Campux qui ont eu un grand succès. 

== Le LUG de la métropole lilloise : 2004-2005 ==

Le troisième conseil d’administration se compose de Jiel (président), Tony (secrétaire), Matěj (trésorier), MrTom, Jo l’apache et Coraline. Cette nouvelle année va en fait consolider les très bons débuts de l’association. Campux agit sur tous les fronts, fait de nombreuses install parties, participe à divers événements (Salon du jeu vidéo, Journée du libre à X2000, brocante de Proville) et aux événements de la communauté (Linux Solutions à Paris, FOSDEM à Bruxelles, RMLL à Dijon). Le site fait peau neuve, principalement grâce à Matěj et jux.

En parallèle, Campux se voit confier la direction de l’opération ministérielle Micro-portable étudiant (MIPE) par l’Université de Lille1. Campux conseille les étudiants à propos de l’achat des machines et du wifi. Ceci permet à l’association d’obtenir un local (qu’elle partage cependant avec les mutuelles étudiantes) dans le bâtiment DEUG sur le campus de la fac. Trois jours par semaine l’association fait des permanences pour aider les étudiants et les autres ! On retrouve ainsi très fréquemment Tony, guigui, jo, Jiel, MrTom, smig, Matěj… L’association agit sur la métropole et nombreux sont ses membres extérieurs au monde étudiant : il y a désormais une cinquantaine de campuxiens. Des conférences sont organisées à Lille lors des « Mardi du Libre », qui attirent tous les mois davantage de monde. 

== La transition : 2005-2006 ==

Une nouvelle année, un nouveau CA : Tony (président), MrTom (secrétaire), Matěj (trésorier), chtitux, FBI_Pierreo, Champignon, Jiel et nemy. L’association continue les mardis et quelques activités. C’est année qui commence tard, avec un CA fin octobre. Une année qui commence assez mal aussi : une journée du Logiciel Libre décalée 3 fois : d’abord prévue mi-décembre, elle se tient fin janvier et constitue un échec pour l’association. Dans la foulée l’hébergeur de campux.org disparait dans un crash de serveur et le site avec lui.

Ces temps troublent voient la perte de l’ancienne liste des adhérents, dont de toutes façons pour la plupart l’association était sans nouvelles.

Littlecharly (alias bannaneverte) modernise, malgré les troubles, la charte graphique de l’association. Rouge et blanc : aux couleurs de Lille. Le beffroi symbolisant la ville de Lille (même si d’autres personnes y verront des choses parfois surprenantes). Des flyers sont créés, et la renaissance est en route 

== 2006 - 2007 : de Campux à Chtinux ==

À la rentrée 2006 c’est l’électrochoc : l’association se réorganise et chacun y met du sien. L’association redevient très active : une nouvelle rencontre est organisée tous les derniers mardis du mois au café citoyen, le site web est recréé (à l’initiative de chtitux), la participation à Mix’Cité est une réussite et les anciens souvent peu (ou pas) présents sont remplacés par les nouveaux membres.

L’association continue sur sa lancée avec un nouveau bureau élu fin 2006. La nouvelle équipe sera composée pour 2006-2007 de Philippe Pary (Pustule) en tant que président, Thomas Canniot (MrTom) en tant que secrétaire, Karl Leicht (Nuln) en tant que trésorier, Tony Ducrocq (dtony) en tant que vice-président, Théophile Helleboid (Chtitux) en tant que vice-secrétaire et Charles Vinchon (LittleCharly) en tant que vice-trésorier.

Pour une bonne partie il s’agit de nouveaux membres de Campux, une page d’histoire tourne donc inévitablement.

L’association continue d’organiser régulièrement des évènements tels que les Mardis du Libre et les derniers mardis au café citoyen. Elle sert aussi de relais local de l’APRIL pour les évènements d’envergure nationale. 

== Linux59 ? Linux Lille ? Chtinux ! ==

En 2007, l’association voit ses activités sur la factulté de Lille déliner alors que ses activités en dehors du campus se multiplient et ont une portée de plus en plus forte.

Le nom Campux sème alors la confusion. De nombreux interlocuteurs ne voient en Campux qu’une association étudiante. Le besoin de changer de nom devient une réalité concrète et urgente. Cédric Durmont proposera même de fonder une association sur Wambrechies, persuadé de Campux est une association purement universitaire !

Changer de nom, oui, mais lequel prendre ?

C’est Virginie Couture qui apportera la réponse avec Chtinux. Le nom est un double abus de langage : le Chti couvre bien plus que la métropole et Linux ne désigne pas tous les logiciels Libres. Cependant ce nom indique clairement l’origine et l’objet de l’association.

Il sera retenu lors d’une assemblée générale extraordinaire tenue le 31 juillet 2007.

Ce sera également Virginie qui créera le logo de l’association : le tux avec un cornet de frites.

== 2007-2008 : l’association grandit ==

En septembre 2007 pour la seconde année Chtinux participe à la braderie. Ubuntu-fr, Linux Cambrésis, Cliss XXI et l’April sont également présents sur 4 mètres de stands. L’évènement est un succès. Le mois qui suivra la braderie verra plus d’une dizaine de nouvelles adhésions à l’association. À l’occasion de la braderie Chtinux distribue pour la première fois son CD Chti’Libre

Début août 2007, Chtinux compte une petite trentaine d’adhérents. Un an plus tard, l’association compte 75 adhérents.

Chtinux a multiplié les évènements. Ils sont au rythme de 3 par mois :

Conférence le second mardi du mois à Bois Blancs Install Party, notamment avec le soutien de la MRES La permanence associative le dernier mardi de chaque moi au café citoyen

Ces évènements, ajoutés à la participation à de nombreux salons/forums/etc., accroissent la réputation et la visibilité de l’association et attirent à elle de très nombreuses personnes (on compte une adhésion tous les 5 visiteurs !).

Pour la première année, Chtinux participe à l’évènement Libre en fête en organisant plusieurs évènements.

Lors des élections municipales de mars, Chtinux tente de s’impliquer dans la campagne en prenant contact avec les candidats (initiative Candidats de l’April). 

== Avenir ==

À suivre…
