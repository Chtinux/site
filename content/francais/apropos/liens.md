---
title: "Des liens vers ce que l'on aime bien!"
meta_title: "Liens"
description: "Chtinux est une association de promotion des logiciels libres de la métropole lilloise"
draft: false
---


À propos : Liens
================

Associations dont Chtinux est membre
------------------------------------

Chtinux est membre des associations suivantes :

*   [APRIL](http://www.april.org/), association de défense et de promotion du logiciel Libre
*   [ANIS](http://www.nord-internet-solidaire.org/), réflexion citoyenne autour des usages d’Internet

Autres Groupes d’utilisateurs de logiciels Libres dans la région Nord-Pas de Calais
-----------------------------------------------------------------------------------

Et autres groupes d’informaticiens plus ou moins barbus et plus ou moins de la région :

*   [Béthune Libre](http://bethunelibre.tuxfamily.org/), Béthune
*   [CLX](http://clx.asso.fr/), régional
*   [Linux Cambrésis](http://www.linuxcambresis.org/)
*   [Linux62](http://www.linux62.org/), Boulogne sur Mer

Associations d’informatique libre à Lille
-----------------------------------------

*   [Mycélium](https://mycelium-fai.org/), fournisseur d’accès internet membre de la fédération FFDN dans la région lilloise
*   \[Raoull\]([https://raoull.org/](https://raoull.org/), hébergement de services mutualisés pour un internet décentralisé (CHATONS)

Culture et informations locales
-------------------------------

*   [Le café citoyen](https://cafecitoyen.org)
*   [La MRES](https://mres-asso.org)
*   [Léa Linux](https://lea-linux.org) site d’entraide autour de Linux

Prestataires de services informatiques
--------------------------------------

*   [Cliss XII](https://www.cliss21.com/), coopérative en logiciels Libres (Liévin/Lille)
*   [Si7v](https://www.si7v.fr/), une SSLL d’Hesdin

Sites communautaires de distributions
-------------------------------------

*   [Debian](https://www.debian.org/), une distribution Linux porteuse de valeurs communautaires
*   [Fedora France](https://www.fedora-fr.org), dont des membres de Chtinux font partie
*   [Mageia](http://www.mageia.org/fr/), une distribution Linux française
